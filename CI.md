---
title: CI setup for Migamake API for CloudFlare
---

# CI

This project uses [Gitlab CI](https://docs.gitlab.com/ee/ci/), which uses the `.gitlab-ci.yml` file 
to define the CI jobs.

All stages are setup to cross-build the package for both Scala 2.12 and 2.13.

## Build
This stage cleans the workspace, and compiles the package.

## Test
Runs the unit and integration tests.

The integration tests involve adding, filtering and removing DNS records from an active DNS zone.

The Cloudflare API settings are configured using three environment variables.

- `CLOUDFLARE_TOKEN`. A Cloudflare API authorisation token.
- `CLOUDFLARE_ZONE`. ID of the a Cloudflare zone ID to test DNS records.
- `CLOUDFLARE_DOMAIN`. The domain name to use, in the format example.com

Please note that your deployment runs on a tag. If you protect your variables,
you should include `^v[0-9.]+$` to the list of protected tags in GitLab.

## Docs
This stage builds the documentation.

## Deploy
### Release Process
The project uses the [sbt-release](https://github.com/sbt/sbt-release) plugin
to manage the release.

The release plugin runs in non-interactive mode, using the default plugin values. 
For example, if the package is at version 1.0-SNAPSHOT, the plugin will release 1.0, and bump the 
version up to 1.1-SNAPSHOT.

The release is split into three steps; 

- `sbt prepareRelease`. 
  - Checks the build does not have any SNAPSHOT dependencies.
  - Sets `version.sbt` to the release version. The release version is obtained by dropping `-SNAPSHOT` from 
  the version in `version.sbt`.
  - Commits the file. The commit message will be 'Setting version to <version>'.
  - Tags the commit. The tag will be 'v<version>'.
  - Pushes the changes to git, which will trigger a CI pipeline run.
- `sbt performRelease`. 
    - Checks the build does not have any SNAPSHOT dependencies.
    - Builds signed artifacts for all supported Scala versions using `sbt publishSigned`.
    - Uploads artifacts to Sonatype using `sbt sonatypeBundleRelease`.
- `sbt postRelease`. 
  - Sets `version.sbt` to the next version. The next version is obtained by increasing the minor
    component, and adding `-SNAPSHOT`. For example, 1.0 -> 1.1-SNAPSHOT. Note that if the current
    version is already a snapshot, no action is performed.
  - Commits the file. The commit message will be 'Setting version to <version'>.
  - Pushes the changes. The changes are pushed using the `ci.skip` option to avoid triggering 
  the CI pipeline.

Note that the interactive mode of the sbt-release plugin is not enabled. To manually set the relase version,
it should be committed to `version.sbt` before running the release.

### Syncing to Maven Central
This project uses the [sbt-pgp](http://www.scala-sbt.org/sbt-pgp/) plugin to sign artifacts,
and the [sbt-sonatype](https://github.com/xerial/sbt-sonatype) to handle uploading the artifacts.

The release process will generate signed artifacts using a GPG key for each supported Scala version.
This will be placed in a local staging repository, and then uploaded to a remote staging repository.
Finally, the remote repository will be promoted to a release, and the closing repository will be closed. 
This process is handled by the `publishSigned` and `sonatypeBundleRelease` commands.

Deploying requires the following environment variables to sign the package and authenticate with Sonatype.

- `SONATYPE_REPO_USERNAME`. Username to access the Sonatype repo (not email!)
- `SONATYPE_REPO_PASSWORD`. Password to access the Sonatype repo.
- `GPG_SECRET_KEY`. The exported private PGP key.
- `PGP_PASSPHRASE`. The passphrase used when creating the PGP key.

# Troubleshooting

1. `401` - check that `SONATYPE_REPO_USERNAME` is indeed username, not email. `SONATYPE_REPO_PASSWORD` should be set.
2. `Cannot find staging` - check that `sonatypeProfileName` in `*.sbt` files is set to your artifact name (like `com.migamake.api.cloudflare`
3. _Cannot find version staging directory_ or artifacts do not appear in Sonatype repo, but repository itself is created
4. _Signing problems_ - the `sbt-pgpsign` seems to require both setting `GPG_SECRET_KEY` as file variable **and** `PGP_PASSPHRASE` to work.
