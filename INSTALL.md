# Installation instructions
This document describes how to build and test the library, as well as an overview of the code.

Note that most commands are prefixed with `+`, which tells SBT to run 
the command for all Scala versions the library supports (i.e. 2.12 and 2.13).

During development, you may wish to omit the `+` and simply run commands on Scala 2.13.

## Building
### Compiling
`sbt +compile`

### Build documentation
`sbt +doc`

Scaladocs will be compiled to `target/scala-2.xx/api`.

### Publish local package
This creates a package in your local ivy repository (`~/.ivy2/local/`).

`sbt +publishLocal`

To create signed artifacts, the below command should be used. Note that this command requires the
GPG program to be installed, and a key must have been generated.

`sbt +publishSigned`

## Testing

Testing requires three environment variables to be set:
- `CLOUDFLARE_TOKEN`. The Cloudflare API authorisation token.
- `CLOUDFLARE_ZONE`. ID of the zone.
- `CLOUDFLARE_DOMAIN`. Domain name (e.g example.com).

SBT tests are run using `sbt test`. Since it's not possible to pass 
environment variables directly to `sbt test`, they should first be exported as below.

```
export CLOUDFLARE_TOKEN=auth-key
export CLOUDFLARE_ZONE=zone-id
export CLOUDFLARE_DOMAIN=example.com
sbt +test
```

### Verbose mode

Verbose mode can be enabled to print all requests, as well as any existing DNS records found at the 
start of the test.

Verbose can be enabled by setting an environment variable to true. In case the variable is not present,
or cannot be parsed as a boolean, verbose mode be disabled.

- `VERBOSE_TESTS`


## Publishing
This package is setup to publish to Maven Central, and uses the [sbt-release](https://github.com/sbt/sbt-release) plugin to manage releasing.

The release also uses the [sbt-sonatype](https://github.com/xerial/sbt-sonatype) to publish the package to Sonatype.

The release is split into three steps, which are detailed in [CI.md](CI.md). The first stage commits & tags the version, 
the second stage builds and uploads the artifacts, while the last stage bumps the version to the next snapshot.

If releasing manually, all steps should be called. 

```sbt
sbt prepareRelease
sbt performRelease
sbt postRelease
```

Note that the release commands are not prefixed with `+`. Several steps of the release process 
should only run once, so the release plugin manages cross-building automatically. In `build.sbt`,
you will see we have set `releaseCrossBuild` to true. 

This runs an non-interactive release, which means the user is not prompted for information such confirming the release version.
Interactive mode is not enabled, so to release a non-standard version (for example, a major version bump), the 
change should be commited to `version.sbt` before calling the release. 

In addition, the package is signed using the [sbt-pgp](https://github.com/sbt/sbt-pgp) plugin.

This means a developer releasing the package must have a PGP key available on their device.

The following environment variables must also be set when releasing from a local device.

- SONATYPE_REPO_USERNAME. Username for the Sonatype repo.
- SONATYPE_REPO_PASSWORD. Password for the Sonatype repo.

When releasing from Gitlab, these environment variables must also be set.

- `GPG_SECRET_KEY`. The exported private PGP key.
- `PGP_PASSPHRASE`. The passphrase used when creating the PGP key. Without passphrase, `publishSigned` will fail with an error.

Note that in order to use the key, it must be masked. [See here](https://docs.gitlab.com/ee/ci/variables/README.html#mask-a-custom-variable)
for more information on providing environment variables, and for using [custom environment variables](https://docs.gitlab.com/ee/ci/variables/#custom-environment-variables-of-type-file) 
of type File.


(Workflow for publishing manually)[https://github.com/xerial/sbt-sonatype/blob/master/workflow.md].

## Code overview
The package uses the [sttp](https://sttp.softwaremill.com/en/latest/) library to make 
HTTP requests, and the [Play JSON](https://github.com/playframework/play-json) library to handle
JSON serialization.

One of the benefits of using `sttp` is the ease of using different implementations to make the HTTP request. 
In the `sttp` library these implementations are called backends. 
This makes it easy to include both a synchronous and asynchronous client using the same base client.

The code is split into two packages; `com.migamake.cloudflare.api.client` and 
`com.migamake.cloudflare.api.model`. The model package contains the API models, 
while the client package contains the clients.

### Clients
#### `BaseCloudflareDnsClient`
This trait is the base client, which is not exposed outside of the package. 
It handles making the request, and deserializing the response. 

Classes which implement the base client must provide an `sttp` backend, and a type constructor. 
Type constructors used in this library are `Future`and `Identity`.

#### `SyncCloudflareDnsClient`
This is a client which uses a synchronous backend (`HttpURLConnectionBackend`). Note that this 
client overrides the methods of `BaseCloudflareDnsClient`, while simply calling the super method. 

If we did not override the API methods, they would return `Identity[X]` to the caller.
There's no practical downside of this, since `Identity[X] = X`, however it does cause the caller
extra effort as they would need to look into the code and understand why this is the case. 
Instead, by overriding the methods we can avoid exposing the `Identity` type and achieve a cleaner API.

#### `AsyncCloudflareDnsClient`
A client which uses an asynchronous backend (`AsyncHttpClientFutureBackend`). 

This backend is non-blocking, and returns a `Future`.

### Models
#### `ClientResponse`
The base type returned from each method. It is a wrapper for the Cloudflare response,
which adds the response code and response headers.

This contains the deserialized body, as well as the raw JSON body.

#### `CloudflareResponseContainer`
The standard response format for the Cloudflare API. As well as the requested data,
it includes a success flag, paging information, and possible error messages.

#### `DnsRecord`
The schema for a DNS record returned by the Cloudflare API.

#### `CreateDnsRecordData`
The required properties to make a new DNS record.

