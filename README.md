---
title: Migamake Scala client for CloudFlare DNS
---
# Migamake Scala Client for Cloudflare DNS
This package provides a Scala client to add, remove and view DNS records using the Cloudflare API.

It provides both synchronous and asynchronous clients, and data 
structures based on the Cloudflare API.

## Usage
### Adding dependency
#### Scala
The package is built for both Scala 2.12 and 2.13. It can be added to `build.sbt` as follows:
```scala
libraryDependencies += "com.migamake" %% "migamake-cloudfare-api" % "1.0.0"
```

### Creating a client
A client must be provided the **authorisation token** when instantiated. This token should have DNS edit 
permissions enabled to create and delete records.

A default base URL of `https://api.cloudflare.com/client/v4` is used, which can also be changed when 
creating the client if needed.

#### Scala
The synchronous client can be instantiated as below.

```scala
val syncClient = new SyncCloudflareDnsClient(authToken = "auth-token")
```

The asynchronous client requires an `ExecutionContext` in scope. 
For example, when using the global execution context, the client 
could be instantiated as below.

```scala
import scala.concurrent.ExecutionContext.Implicits.global
val asyncClient = new AsyncCloudflareDnsClient(authToken = "auth-token")
```

#### Java
The synchronous Java client can be instantiated as below.
```java
SyncCloudflareDnsClient client = new SyncCloudflareDnsClient("auth-token");
```

The asynchronous client requires an `ExecutionContext` to be provided.
Below is an example using the global execution context.
```java
ExecutionContext ec = scala.concurrent.ExecutionContext.global();
AsyncCloudflareDnsClient client = new AsyncCloudflareDnsClient("auth-token", ec);
```

The asynchronous client returns instances of `scala.concurrent.Future`. If you are using Scala 2.13,
this can be converted to a `java.util.Future` using the method 
`scala.jdk.javaapi.FutureConverters.asJava(scalaFuture)`.

### Listing DNS entries
DNS entries for a zone can be listed, with optional filters and sorting applied.

The request parameters are supplied using the `ListDnsRequestParams` class.

All properties have default values corresponding to the [Cloudflare API documentation](https://api.cloudflare.com/#dns-records-for-a-zone-list-dns-records).
The default case retrieves the first 20 DNS records, with no filtering applied.

#### Scala
In the example below, the synchronous client is used to list all `A` records.

```scala
val syncClient = new SyncCloudflareDnsClient(authToken = "auth-token")
val params = ListDnsRequestParams(dnsType = Some(DnsRecordType.A))
val results: ClientResponse[CloudflareResponseContainer[List[DnsRecord]]] = 
  syncClient.listDnsRecords(zoneId = "zone-id", requestParams = params)
```

#### Java
Since Java cannot use the default arguments in the `ListDnsRequestParams` constructor, it is  
convenient to use the `ListDnsRequestParamsBuilder` class to build the 
request parameter object.

This class is immutable, so calls to set a property return a modified object, while leaving the
original instance unchanged.

Below is an example to list all A type records.

```java
SyncCloudflareDnsClient client = new SyncCloudflareDnsClient("auth-token");
ListDnsRequestParamsBuilder builder = new ListDnsRequestParamsBuilder();
ListDnsRequestParams params = 
        builder
            .setDnsType(Option.apply(DnsRecordType.A()))
            .build();
ClientResponse<CloudflareResponseContainer<List<DnsRecord>>> results = 
        client.listDnsRecords("zone-id", params);
```

Notice that the builder does not expect null values, and instead uses `scala.Option`.

### Adding a DNS record
DNS records can be added using the `CreateDnsRecordData` class, which provides the properties
required by the API to create a record.

As per the [Cloudflare documentation](https://api.cloudflare.com/#dns-records-for-a-zone-create-dns-record),
the following properties are required to create a DNS record.

- Type
- Name
- Content
- TTL

There are optional properties which may also be provided.

- Priority
- Proxied

#### Scala
Below is an example to add a TXT record.
```scala
val data = CreateDnsRecordData(
  dnsType = DnsRecordType.TXT,
  name = "foo",
  content = "bar",
  ttl = 1
)
val result: ClientResponse[CloudflareResponseContainer[DnsRecord]] = 
  client.addDnsRecord("zone-id", data)
```

Below is an example adding an MX record, using all available properties.
```scala
val data = CreateDnsRecordData(
  dnsType = DnsRecordType.MX,
  name = "foo",
  content = "bar",
  ttl = 1,
  priority = Some(1),
  proxied = Some(true)
)
val result: ClientResponse[CloudflareResponseContainer[DnsRecord]] = 
  client.addDnsRecord("zone-id", data)
```

#### Java
Below is an example to add a TXT record.
```java
CreateDnsRecordData data = new CreateDnsRecordData(DnsRecordType.TXT(), "name", "content", 1);
ClientResponse<CloudflareResponseContainer<DnsRecord>> results = client.addDnsRecord("zone-id", data);
```

Below is an example adding an MX record, using all available propertis.
```java
CreateDnsRecordData data = new CreateDnsRecordData(
        DnsRecordType.MX(), 
        "name", 
        "content", 
        1, 
        Option.apply(1), 
        Option.apply(false)
);
ClientResponse<CloudflareResponseContainer<DnsRecord>> results = client.addDnsRecord("zone-id", data);
```

### Deleting a DNS record
DNS records can be deleted using the clients delete method. Cloudflare responds with the record ID if
it was successfully deleted.

#### Scala
```scala
val result: ClientResponse[CloudflareResponseContainer[IdObject]] = 
  client.deleteDnsRecord(zoneId = "zone-id", dnsId = "dns-id")
```

#### Java
```java
ClientResponse<CloudflareResponseContainer<IdObject>> result = 
        client.deleteDnsRecord("zone-id", "dns-id");
```

## Responses
All responses are wrapped in the class `ClientResponse`. This gives access to the 
response code, headers, the raw response body and the deserialized response body.

Responses are deserialized as instances of `CloudflareResponseContainer`, which
exposes the standard API response structure used by Cloudflare. This includes error messages, 
a flag indicating if the response was a success, as well as the requested data.

The synchronous client may throw exceptions for network-level exceptions. For example, 
if the host cannot be reached. The asynchronous client returns a failed `Future` rather 
than throwing exceptions.

Requests which return successfully do not throw exceptions – even when the response code 
is 4xx, 5xx, or the body cannot be deserialized. These cases can be detected by inspecting 
the response object.

Below are further usage examples, using the response body.

### Errors
#### HTTP errors

HTTP errors throw an exception, which will propagate in the case of the synchronous client,
and be returned as a failed `Future` for the asynchronous client.

Note that common network level exceptions are wrapped in an [`SttpClientException`](https://sttp.softwaremill.com/en/latest/responses/exceptions.html),
which has two subclasses: `ConnectException` and `ReadException`

##### Scala
Below is an example of detecting network level exceptions, with specific handling of `UnknownHostException`.
```scala
val client = new SyncCloudflareDnsClient(authToken = "auth-token", baseUrl = "http://a.b.c")

try {
  client.deleteDnsRecord(zoneId = "zone-id", dnsId="dns-id")
} catch {
  case err: SttpClientException.ConnectException =>
    if (err.getCause.isInstanceOf[UnknownHostException])
      println("Unknown host")
    else
      println(s"Error connecting: ${err.getMessage}")

  case err: SttpClientException.ReadException =>
    println(s"Error reading response ${err.getMessage}")
    
  case err: Throwable => 
    println(s"Unknown error ${err}")
}
```

##### Java
```java
SyncCloudflareDnsClient client =
        new SyncCloudflareDnsClient("auth-token", "http://a.b.c");
try {
    client.deleteDnsRecord("zone-id", "dns-id");
} catch (SttpClientException.ConnectException err) {
    if (err.getCause() instanceof UnknownHostException) {
        System.out.println("Unknown host");
    } else {
        System.out.println("Error connecting: " + err.getMessage());    
    }
} catch (SttpClientException.ReadException err) {
    System.out.println("Error reading request: " + err.getMessage());
} catch (Exception err) {
    System.out.println("Unknown exception " + err);
}
```

#### Body deserialization failures

There are two ways the body may fail to deserialize.

- The body fails to be read into JSON.
- The JSON fails to deserialize into the known data structures.

The raw body type is an `Either`, where the `Right` instance is the JSON body, while
the `Left` is an error message indicating why the body could not be read.

In the case the raw body cannot be read, the status code and headers of the response can be inspected.

The deserialized body type is an `Option`, which will be empty when the JSON fails to be deserialized
into the correct type.

Note that body deserialization errors are total. Which means if any part of the body fails to deserialize, 
the entire body will fail. For example, when listing DNS records, if one record cannot be read, the 
entire body will fail to be read. Similarly, if the response container can be read but the result itself cannot,
the entire body will fail to be read.

##### Scala
````scala
val result: ClientResponse[CloudflareResponseContainer[IdObject]] = 
  client.deleteDnsRecord(zoneId = "zone-id", dnsId = "dns-id")
  
// Print the raw JSON body, or an error, status code and headers.
result.rawBody match  {
  case Right(rawJson) => 
    println(rawJson)
  case Left(error) => 
    println(s"Error reading body: $error")
    println(result.responseCode)
    println(result.headers)
}
    
// Print the deserialized body, or an error
result.body match  {
  case None => 
    println(s"Error deserializing body")
  case Some(value: CloudflareResponseContainer[IdObject]) => 
    println(value)
}
````

##### Java
```java
ClientResponse<CloudflareResponseContainer<IdObject>> result = 
        client.deleteDnsRecord("zone-id", "dns-id");

// Print the raw JSON body, or an error, status codes and headers.
if (result.rawBody().isRight()) {
    String rawJson = result.rawBody().right().get();
    System.out.println(rawJson);
} else {
    String left = result.rawBody().left().get();
    System.out.println("Error reading body: " + left);
    System.out.println(result.responseCode());
    System.out.println(result.headers());
}

// Print the deserialized body, or an error
if (result.body().isDefined()) {
    CloudflareResponseContainer<IdObject> body = result.body().get();
    System.out.println(body);
} else {
    System.out.println("Could not read body");
}
```

#### Cloudflare Errors

Cloudflare responses are wrapped in the class `CloudflareResponseContainer[T]`, which provides
a success flag, errors, additional messages, paging information, and finally the result, which
is of type `Option[T]`.

Responses which are expecting a result should detect failures by the lack of a result field, 
since the failed request will not return a result.

Note that an empty result does not indicate that it has failed to deserialize. As mentioned above, 
should the result fail to deserialise, the entire body (i.e., including the container) will also fail.

The Cloudflare response also includes a success flag, which may also be used to detect a failure.

In the case of a failure, the response code and error messages can be used to detect the reason.

Cloudflare errors contain an error code - the codes specific to managing DNS records within a 
zone can be [found here](https://api.cloudflare.com/#dns-records-for-a-zone-export-dns-records).

Note that this is not an exhaustive list of all possible error codes, but only those specific 
to DNS management in a zone. For example, there are also error codes related to failed authorisation.

##### Scala
```scala
val result: ClientResponse[CloudflareResponseContainer[IdObject]] =
  client.deleteDnsRecord(zoneId = "zone-id", dnsId = "dns-id")
val body: CloudflareResponseContainer[IdObject] = result.body.get
  
if (body.result.isEmpty) {
  println(result.responseCode)
  val errors: List[CloudflareError] = body.errors
  errors.foreach { error =>
    println(s"${error.code} : ${error.message}")
  }
}
```

##### Java
```java
import scala.jdk.javaapi.CollectionConverters;
ClientResponse<CloudflareResponseContainer<IdObject>> result =
        client.deleteDnsRecord("zone-id", "dns-id");
CloudflareResponseContainer<IdObject> body = result.body().get();

if (body.result().isEmpty()) {
    System.out.println(result.responseCode());
    List<CloudflareError> errors = body.errors();
    java.util.List<CloudflareError> javaErrorsList = CollectionConverters.asJava(errors);
    for (CloudflareError error: javaErrorsList) {
        System.out.println(error.code() + " : " + error.message());
    }
}
```

### Further examples
#### Print a summary of DNS records for a subdomain
Below is an example of printing a few fields for the first 100 DNS records matching a given name. Note that
100 is the maximum number of DNS records which can be returned per page.

The total number of pages is also printed.
  
##### Scala

```scala
val client = new SyncCloudflareDnsClient(authToken = "auth-token")
val result: ClientResponse[CloudflareResponseContainer[List[DnsRecord]]] = client.listDnsRecords(
  zoneId = "zone-id",
  requestParams = ListDnsRequestParams(
    name = Some("foo.example.com")
  )
)

for {
  body: CloudflareResponseContainer[List[DnsRecord]] <- result.body
  result: List[DnsRecord] <- body.result
} yield {
  result.foreach { result => 
    println(result.id)
    println(result.dnsType)
    println(result.name)
    println(result.content)
    println(result.ttl)
  }
}

for {
  body <- result.body
  paging <- body.resultInfo
} yield {
  println(s"There are ${paging.totalPages} total pages")
}
```

##### Java
```java
import scala.jdk.javaapi.CollectionConverters;
ListDnsRequestParamsBuilder builder = new ListDnsRequestParamsBuilder();
ListDnsRequestParams params = builder
        .setName(Option.apply("foo.example.com"))
        .build();

ClientResponse<CloudflareResponseContainer<List<DnsRecord>>> result =
        client.listDnsRecords("zone-id", params);

if (result.body().isDefined()) {
    if (result.body().get().result().isDefined()) {
        List<DnsRecord> records = result.body().get().result().get();
        java.util.List<DnsRecord> javaRecordsList = CollectionConverters.asJava(records);
        for (DnsRecord record: javaRecordsList) {
            System.out.println(record.id());
            System.out.println(record.dnsType());
            System.out.println(record.name());
            System.out.println(record.content());
            System.out.println(record.ttl());
        }
    }

    if (result.body().get().resultInfo().isDefined()) {
        ResultInfo paging = result.body().get().resultInfo().get();
        System.out.println("There are " + paging.totalPages() + " total pages");
    }
}

```
