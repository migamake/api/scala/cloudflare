import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._
import scala.sys.process._

name := "migamake-cloudflare-api"

scalaVersion := "2.13.3"

crossScalaVersions := Seq("2.12.12", scalaVersion.value)

// STTP client and play-json
libraryDependencies += "com.softwaremill.sttp.client" %% "core" % "2.2.9"
libraryDependencies += "com.softwaremill.sttp.client" %% "play-json" % "2.2.9"
libraryDependencies += "com.softwaremill.sttp.client" %% "async-http-client-backend-future" % "2.2.9"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.9.1"

// This is needed as the async sttp client requires an SLF4J implementation, and
// prints an error to stderr when one is not found.
libraryDependencies += "org.slf4j" % "slf4j-nop" % "1.7.30"

// ScalaTest and parallel collections lib for testing
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.2" % "test"
libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % Test
libraryDependencies ++= {
  CrossVersion.partialVersion(scalaVersion.value) match {
    case Some((2, major)) if major <= 12 =>
      Seq()
    case _ =>
      Seq("org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.0-RC1" % "test")
  }
}

// Enables stdout for successful tests, and ANSI colors
testOptions += Tests.Argument(TestFrameworks.JUnit, "+q", "+s")

// Enables fatal compiler warnings
val scala2_13CompilerOptions = Seq(
  "-Werror",
  "-Wdead-code",
  "-Xlint:unused",
  "-Xlint:missing-interpolator",
  "-Xlint:deprecation",
  "-Ywarn-macros:after"
)

val scala2_12CompilerOptions = Seq(
  "-Ywarn-unused:imports",
  "-Ywarn-dead-code",
  "-Ywarn-unused:params",
  "-Ywarn-unused:locals",
  "-Ywarn-unused:privates",
  "-Ywarn-macros:after",
  "-Xfatal-warnings",
  "-language:higherKinds",
  "-deprecation",
  "-feature",
  "-unchecked"
)

scalacOptions ++={
  CrossVersion.partialVersion(scalaVersion.value) match {
    case Some((2, major)) if major <= 12 =>
      scala2_12CompilerOptions
    case _ =>
      scala2_13CompilerOptions
  }
}

// Releasing settings
// Artifact groupId:
//pomhomepage in ThisBuild := Some(url("https://gitlab.com/migamake/api/scala/cloudflare"))
organization in ThisBuild := "com.migamake.api.cloudflare"
organizationName in ThisBuild := "Migamake Pte Ltd"
organizationHomepage in ThisBuild := Some(url("https://migamake.com"))
description in ThisBuild := "A Scala client to view, add and delete DNS records using the Cloudflare API"
homepage in ThisBuild := Some(url("https://gitlab.com/migamake/api/scala/cloudflare"))
publishMavenStyle in ThisBuild := true
releaseIgnoreUntrackedFiles in ThisBuild := true
releaseIgnoreUntrackedFiles := true
homepage in ThisBuild := Some(url("https://gitlab.com/migamake/api/scala/cloudflare"))
//sonatypeProjectHosting := Some(GitLabHosting("migamake/api/scala", "cloudflare", "mjgajda@migamake.com"))
//sonatypeProfileName := "migamake@migamake.com"
sonatypeProfileName in ThisBuild := "com.migamake.api.cloudflare"

publishTo in ThisBuild := sonatypePublishToBundle.value

def getEnvOrDefault(env: String, default: String): String = {
  Option(System.getenv(env)) match {
    case Some(value) => value
    case None =>
      println(s"Could not find environment variable $env")
      default
  }
}

credentials += Credentials(
  "Sonatype Nexus Repository Manager",
  "oss.sonatype.org",
  getEnvOrDefault("SONATYPE_REPO_USERNAME", ""),
  getEnvOrDefault("SONATYPE_REPO_PASSWORD", "")
)

scmInfo := Some(
  ScmInfo(
    url("https://gitlab.com/migamake/migamake-hurl/cloudflare-java-api-package"),
    "scm:git@gitlab.com:migamake/migamake-hurl/cloudflare-java-api-package.git"
  )
)

licenses := Seq("BSD3" -> url("https://opensource.org/licenses/BSD-3-Clause"))

developers := List(
  Developer(id="mjgajda", name="Michal Gajda", email="scala-dev@migamake.com", url=url("https://gitlab.com/mjgajda"))
)

val pushCommitsSkipCi = ReleaseStep(action = st => {
  val pushResult = "git push -o ci.skip".!
  if (pushResult != 0) {
    sys.error(s"Error when pushing to git. The exit code was $pushResult")
  }
  st
})

// This uses the default release process, but uses PGP signing and the sonatypeReleaseBundleRelease command
releaseCrossBuild := true // true if you cross-build the project for multiple Scala versions

commands += Command.command("prepareRelease")((state: State) => {
  println("Preparing release")
  val extracted = Project.extract(state)
  val stateWithReleaseProcess = extracted.appendWithSession(
    Seq(
      releaseProcess := Seq(
        checkSnapshotDependencies,
        inquireVersions,
        setReleaseVersion,
        commitReleaseVersion,
        tagRelease,
        pushChanges
      )
    ),
    state
  )
  Command.process("release with-defaults", stateWithReleaseProcess)
})

commands += Command.command("performRelease")((state: State) => {
  println("Performing release")
  val extracted = Project.extract(state)
  val stateWithReleaseProcess = extracted.appendWithSession(
    Seq(
      releaseProcess := Seq(
        checkSnapshotDependencies,              // : ReleaseStep
        runClean,                               // : ReleaseStep
        releaseStepCommandAndRemaining("+publishSigned"),
        releaseStepCommand("sonatypeBundleRelease"),
      )
    ),
    state
  )
  Command.process("release with-defaults", stateWithReleaseProcess)
})

commands += Command.command("postRelease")((state: State) => {
  println("Completing release")
  val extracted = Project.extract(state)
  val version = extracted.get(Keys.version)
  if (version.endsWith("-SNAPSHOT")) {
    println("Ignored postRelease command as current version is a snapshot.")
    state
  } else {
    val stateWithReleaseProcess = extracted.appendWithSession(
      Seq(
        releaseProcess := Seq(
          inquireVersions,
          setNextVersion,
          commitNextVersion,
          pushCommitsSkipCi,
        )
      ),
      state
    )
    Command.process("release with-defaults", stateWithReleaseProcess)
  }
})

// Linters
wartremoverErrors ++= Warts.unsafe
scapegoatVersion in ThisBuild := "1.4.6"
usePgpKeyHex("CB079A9CABE4274534F207A44B2DE48C4DD8B861")
