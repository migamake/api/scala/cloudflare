import xerial.sbt.Sonatype._

publishMavenStyle := true

//sonatypeProfileName := "migamake"
sonatypeProfileName := "com.migamake.api.cloudflare"
sonatypeProjectHosting := Some(GitLabHosting(user="migamake/api/scala", repository="scala", email="migamake@migamake.com", fullName="Migamake Pte Ltd"))
developers := List(
  Developer(id = "migamake", name = "Migamake Pte Ltd", email = "migamake@migamake.com", url = url("http://migamake.com/"))
)
licenses := Seq("BSD3" -> url("https://opensource.org/licenses/BSD-3-Clause"))

publishTo := sonatypePublishToBundle.value

//sonatypeLogLevel := "DEBUG"

homepage in ThisBuild := Some(url("https://gitlab.com/migamake/api/scala/cloudflare"))
