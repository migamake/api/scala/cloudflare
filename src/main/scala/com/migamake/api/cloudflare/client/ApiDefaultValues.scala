package com.migamake.api.cloudflare.client

import com.migamake.api.cloudflare.model.enums.MatchType
import MatchType.MatchType

object ApiDefaultValues {
  val baseUrl: String = "https://api.cloudflare.com/client/v4"
  val matchType: MatchType = MatchType.all
  val page: Int = 1
  val perPage: Int = 20
}
