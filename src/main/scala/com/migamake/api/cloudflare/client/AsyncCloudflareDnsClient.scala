package com.migamake.api.cloudflare.client

import sttp.client.SttpBackend
import sttp.client.asynchttpclient.future.AsyncHttpClientFutureBackend

import scala.concurrent.{ExecutionContext, Future}

/**
 * Asynchronous client for listing, adding, and removing DNS records within
 * a Cloudflare DNS zone.
 *
 * All API calls make non-blocking requests, and return a `Future`.
 *
 * The API authorisation token should have permissions to edit DNS records within a zone.
 *
 * Exceptions will be returned as a failed `Future`.
 *
 * Exceptions will be returned for network-level failures. For example, an unreachable host.
 *
 * Protocol-level failures will not return an exception - for example, a request which
 * responds with a 4xx or 5xx status code will not result in an exception.
 * The status code can be obtained from the response object.
 *
 * When ending the application, the close method should be called to free any resources
 *
 * @param authToken The Cloudflare API authorisation token
 * @param baseUrl Base URL of the Cloudflare API
 *                Defaults to "https://api.cloudflare.com/client/v4"
 * @param ec Execution Context to use for the request.
 * @see [[com.migamake.api.cloudflare.client.SyncCloudflareDnsClient]]
 */
class AsyncCloudflareDnsClient(
  val authToken: String,
  val baseUrl: String
)(implicit ec: ExecutionContext) extends BaseCloudflareDnsClient[Future] {

  def this(authToken: String)(implicit ec: ExecutionContext) =
    this(authToken, ApiDefaultValues.baseUrl)

  override private[client] def map[A, B](value: Future[A])(f: A => B): Future[B] =
    value.map(f)

  override private[client] implicit val backend: SttpBackend[Future, Nothing, Nothing] =
    AsyncHttpClientFutureBackend()(ec)

  /**
   * Releases any resources used (if any)
   *
   * This method should be called when ending the application.
   */
  def close(): Future[Unit] = backend.close()
}
