package com.migamake.api.cloudflare.client

import com.migamake.api.cloudflare.model._
import play.api.libs.json.{Json, Reads}
import sttp.client.{SttpBackend, _}

import scala.util.Try

/**
 * The base cloudflare DNS client.
 *
 * Sub-classes must provide an STTP backend.
 *
 * STTP backends support different type constructors, which simplifies
 * re-using the same base client for both synchronous and asynchronous clients.
 *
 * For example, the synchronous client uses an STTP backend with a
 * [[sttp.client.Identity]] type constructor, while the asynchronous client uses
 * an STTP backend with a [[scala.concurrent.Future]] type constructor.
 *
 * @tparam F The type constructor of the STTP backend.
 *           For example, [[scala.concurrent.Future]] or [[sttp.client.Identity]]
 */
private[client] trait BaseCloudflareDnsClient[F[_]] {

  val authToken: String
  val baseUrl: String

  private[client] def map[A, B](value: F[A])(f: A => B): F[B]

  private[client] implicit val backend: SttpBackend[F, Nothing, Nothing]

  private def dnsRecordPath(zoneId: String): String = s"$baseUrl/zones/$zoneId/dns_records"

  private val authRequest =
    basicRequest
      .auth.bearer(authToken)
      .contentType("application/json")

  /**
   * List the DNS records for a zone, with optional filtering and sorting.
   *
   * 20 results per page are returned by default, which can be increased
   * to a maximum of 100.
   *
   * Note that if any DNS record fails to deserialise, the entire body will fail
   * to deserialize. Deserialization failures can be detected in the response body.
   *
   * Returns a status code of 200 for successful requests.
   *
   * @param requestParams Filtering, sorting and ordering paremeters
   * @param zoneId        ID of the DNS zone
   * @return A list of [[com.migamake.api.cloudflare.model.DnsRecord]]
   */
  @SuppressWarnings(Array("org.wartremover.warts.DefaultArguments"))
  def listDnsRecords(
    zoneId: String,
    requestParams: ListDnsRequestParams = ListDnsRequestParams()
  ): F[ClientResponse[CloudflareResponseContainer[List[DnsRecord]]]] = {
    val params: Map[String, Any] = Map[String, Any](
      "match" -> requestParams.matchType,
      "name" -> requestParams.name,
      "order" -> requestParams.order,
      "page" -> requestParams.page,
      "per_page" -> requestParams.perPage,
      "content" -> requestParams.content,
      "type" -> requestParams.dnsType,
      "proxied" -> requestParams.proxied,
      "direction" -> requestParams.direction
    )
    val path = uri"${dnsRecordPath(zoneId)}?$params"
    val response = authRequest
      .get(path)
      .send[F]()
    readDnsRecordResponse[List[DnsRecord]](response)
  }

  /**
   * Adds a new DNS record to the zone.
   *
   * Returns a status code of 200 for successful requests.
   *
   * @param data   Data representing the new record
   * @param zoneId ID of the DNS zone
   * @return The newly created DNS record
   */
  def addDnsRecord(
    zoneId: String,
    data: CreateDnsRecordData
  ): F[ClientResponse[CloudflareResponseContainer[DnsRecord]]] = {
    val path = uri"${dnsRecordPath(zoneId)}"
    val response = authRequest
      .post(path)
      .body(Json.toJson(data).toString())
      .send[F]()
    readDnsRecordResponse[DnsRecord](response)
  }

  /**
   * Deletes a DNS record from the zone.
   *
   * Returns a status code of 200 for successful requests.
   *
   * @param dnsId  The DNS record ID to delete
   * @param zoneId ID of the DNS zone
   * @return ID of the deleted record
   */
  def deleteDnsRecord(
    zoneId: String,
    dnsId: String
  ): F[ClientResponse[CloudflareResponseContainer[IdObject]]] = {
    val path = uri"${dnsRecordPath(zoneId)}/$dnsId"
    val response = authRequest
      .delete(path)
      .send[F]()
    readDnsRecordResponse[IdObject](response)
  }

  /**
   * Deserializes the raw response into a [[com.migamake.api.cloudflare.model.ClientResponse]], using the standard
   * [[com.migamake.api.cloudflare.model.CloudflareResponseContainer]].
   *
   * @param response The raw JSON response
   * @tparam T The body type to read the JSON result as.
   * @return A [[com.migamake.api.cloudflare.model.ClientResponse]] containing the deserialized [[com.migamake.api.cloudflare.model.CloudflareResponseContainer]]
   */
  private def readDnsRecordResponse[T: Reads](
    response: F[Response[Either[String, String]]]
  ): F[ClientResponse[CloudflareResponseContainer[T]]] =
    map(response) { response =>
      val body = for {
        body <- response.body.toOption
        json <- Try(Json.parse(body)).toOption
        deserialized <- json.asOpt[CloudflareResponseContainer[T]]
      } yield deserialized

      ClientResponse(
        response.code.code,
        response.headers.map(h => (h.name, h.value)),
        response.body,
        body
      )
    }
}
