package com.migamake.api.cloudflare.client

import com.migamake.api.cloudflare.model._
import sttp.client._

/**
 * Synchronous client for listing, adding, and removing DNS records within
 * a Cloudflare DNS zone.
 *
 * The API authorisation token should have permissions to edit DNS records within a zone.
 *
 * Exceptions will be thrown for network-level failures. For example, an unreachable host.
 *
 * Protocol-level failures will not result in an exception - for example, a request which
 * responds with a 4xx or 5xx status code will not throw an exception.
 * The status code can be obtained from the response object.
 *
 * @param authToken The Cloudflare API authorisation token
 * @param baseUrl Base URL of the Cloudflare API.
 *                Defaults to [https://api.cloudflare.com/client/v4]
 * @see [[com.migamake.api.cloudflare.client.AsyncCloudflareDnsClient]]
 */
class SyncCloudflareDnsClient(
  val authToken: String,
  val baseUrl: String
) extends BaseCloudflareDnsClient[Identity] {

  def this(authToken: String) = this(authToken, ApiDefaultValues.baseUrl)

  override private[client] implicit val backend: SttpBackend[Identity, Nothing, Nothing] = HttpURLConnectionBackend()

  override private[client] def map[A, B](value: Identity[A])(f: A => B): B = f(value)

  // Note: the reason this client overrides the base client methods is to avoid
  // exposing a return type of `Identity` to the caller.
  // It also allows us to add the @throws annotation.

  /**
   * List the DNS records for a zone, with optional filtering and sorting.
   *
   * 20 results per page are returned by default, which can be increased
   * to a maximum of 100.
   *
   * Note that if any DNS record fails to deserialise, the entire body will fail
   * to deserialize. Deserialization failures can be detected in the response body.
   *
   * Returns a status code of 200 for successful requests.
   *
   * @param requestParams Filtering, sorting and ordering paremeters
   * @return A list of [[com.migamake.api.cloudflare.model.DnsRecord]]
   */
  @throws[Exception]
  @SuppressWarnings(Array("org.wartremover.warts.DefaultArguments"))
  override def listDnsRecords(
    zoneId: String,
    requestParams: ListDnsRequestParams = ListDnsRequestParams()
  ): ClientResponse[CloudflareResponseContainer[List[DnsRecord]]] =
    super.listDnsRecords(zoneId, requestParams)

  /**
   * Adds a new DNS record to the zone.
   *
   * Returns a status code of 200 for successful requests.
   *
   * @param data Data representing the new record
   * @return The newly created DNS record
   */
  @throws[Exception]
  override def addDnsRecord(
    zoneId: String,
    data: CreateDnsRecordData
  ): ClientResponse[CloudflareResponseContainer[DnsRecord]] =
    super.addDnsRecord(zoneId, data)

  /**
   * Deletes a DNS record from the zone.
   *
   * Returns a status code of 200 for successful requests.
   *
   * @param dnsId The DNS record ID to delete
   * @return ID of the deleted record
   */
    @throws[Exception]
  override def deleteDnsRecord(
    zoneId: String,
    dnsId: String
  ): ClientResponse[CloudflareResponseContainer[IdObject]] =
    super.deleteDnsRecord(zoneId, dnsId)
}
