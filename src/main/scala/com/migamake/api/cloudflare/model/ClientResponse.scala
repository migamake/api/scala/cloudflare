package com.migamake.api.cloudflare.model

/**
 * A wrapper for a Cloudflare API response.
 *
 * @param headers Response headers.
 * @param rawBody The raw JSON response from Cloudflare as a `Right`,
 *                or a `Left` with an error message indicating why
 *                the raw body could not be read.
 * @param body    The response body deserialized as type `T`. If the body failed
 *                to deserialize, the body will be `None`.
 * @tparam T The type of the deserialized body.
 */
case class ClientResponse[T](
  responseCode: Int,
  headers: Seq[(String, String)],
  rawBody: Either[String, String],
  body: Option[T]
)
