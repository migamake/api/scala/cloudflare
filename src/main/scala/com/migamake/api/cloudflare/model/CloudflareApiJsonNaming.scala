package com.migamake.api.cloudflare.model

import play.api.libs.json.JsonNaming
import play.api.libs.json.JsonNaming.SnakeCase

/**
 * JSON field naming strategy for the Cloudflare API.
 *
 * Creating a custom naming strategy allows us to avoid using the
 * keyword `type` for field names, while still making use of automatic
 * Play JSON read/writes.
 */
private[model] object CloudflareApiJsonNaming extends JsonNaming {
  override def apply(property: String): String =
    if (property == "dnsType") "type"
    else SnakeCase.apply(property)
}
