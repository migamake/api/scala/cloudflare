package com.migamake.api.cloudflare.model

import play.api.libs.json.{Json, Reads}

/**
 * A Cloudflare error message.
 *
 * @param code    The error code
 * @param message The error message
 */
case class CloudflareError(
  code: Int,
  message: String
)

private[cloudflare] object CloudflareError {
  @SuppressWarnings(Array("org.wartremover.warts.Any"))
  implicit val reads: Reads[CloudflareError] = Json.reads[CloudflareError]
}