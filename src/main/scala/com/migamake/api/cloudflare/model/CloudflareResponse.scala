package com.migamake.api.cloudflare.model

import play.api.libs.functional.syntax._
import play.api.libs.json.JsonConfiguration.Aux
import play.api.libs.json.JsonNaming.SnakeCase
import play.api.libs.json.Reads._
import play.api.libs.json.{JsPath, Json, _}


/**
 * A Cloudflare API response container.
 *
 * @param success    Whether the result was a success
 * @param result     The requested data, if there is any.
 * @param errors     A list of errors, if there are any.
 * @param messages   A list of messages. There is no defined schema for each message,
 *                   so it is defined as a List of `play.api.libs.json.JsValue`
 * @param resultInfo Pagination data, in case the result is paginated.
 * @tparam T The type of the requested data
 */
case class CloudflareResponseContainer[T](
  success: Boolean,
  result: Option[T],
  errors: List[CloudflareError],
  messages: List[JsValue],
  resultInfo: Option[ResultInfo]
)

/**
 * Cloudflare pagination data.
 *
 * @param page       The current page
 * @param perPage    Requested records per page
 * @param count      Count of records on current page
 * @param totalCount Total count of records
 * @param totalPages Total count of pages
 */
case class ResultInfo(
  page: Int,
  perPage: Int,
  count: Int,
  totalCount: Int,
  totalPages: Int
)

private[cloudflare] object ResultInfo {
  private implicit val config: Aux[Json.MacroOptions] = JsonConfiguration(SnakeCase)

  @SuppressWarnings(Array("org.wartremover.warts.Any"))
  implicit val reads: Reads[ResultInfo] = Json.reads[ResultInfo]
}

private[cloudflare] object CloudflareResponseContainer {

  @SuppressWarnings(Array("org.wartremover.warts.Any"))
  implicit def reads[T](implicit fmt: Reads[T]): Reads[CloudflareResponseContainer[T]] = (
    (JsPath \ "success").read[Boolean] and
      (JsPath \ "result").readNullable[T] and
      (JsPath \ "errors").readWithDefault[List[CloudflareError]](List()) and
      (JsPath \ "messages").readWithDefault[List[JsValue]](List()) and
      (JsPath \ "result_info").readNullable[ResultInfo]
    ) (CloudflareResponseContainer.apply[T] _)
}
