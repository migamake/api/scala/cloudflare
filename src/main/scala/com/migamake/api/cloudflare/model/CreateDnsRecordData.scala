package com.migamake.api.cloudflare.model

import com.migamake.api.cloudflare.model.enums.DnsRecordType.DnsRecordType
import play.api.libs.json.JsonConfiguration.Aux
import play.api.libs.json.{Json, JsonConfiguration, OWrites}

/**
 * Data to create a new DNS record.
 *
 * @param dnsType  DNS record type
 * @param name     DNS record name (e.g. example.com)
 * @param content  DNS record content (e.g. 127.0.0.1)
 * @param ttl      Time to live for DNS record. Value of 1 is 'automatic'
 * @param priority Used with some records like MX and SRV to determine priority.
 *                 If you do not supply a priority for an MX record, a default value of 0 will be set
 * @param proxied  Whether the record is receiving the performance and security benefits of Cloudflare
 */
case class CreateDnsRecordData(
  dnsType: DnsRecordType,
  name: String,
  content: String,
  ttl: Int,
  priority: Option[Int],
  proxied: Option[Boolean]
) {

  /**
   * Create a DNS record with no priority, and without Cloudflare proxying
   *
   * @param dnsType DNS record type
   * @param name    DNS record name (e.g. example.com)
   * @param content DNS record content (e.g. 127.0.0.1)
   * @param ttl     Time to live for DNS record. Value of 1 is 'automatic'
   */
  def this(
    dnsType: DnsRecordType,
    name: String,
    content: String,
    ttl: Int
  ) = this(dnsType, name, content, ttl, priority = None, proxied = None)

}

object CreateDnsRecordData {
  private implicit val config: Aux[Json.MacroOptions] = JsonConfiguration(CloudflareApiJsonNaming)
  private[api] implicit val writes: OWrites[CreateDnsRecordData] = Json.writes[CreateDnsRecordData]

  /**
   * Create a DNS record with no priority, and without Cloudflare proxying
   *
   * @param dnsType DNS record type
   * @param name    DNS record name (e.g. example.com)
   * @param content DNS record content (e.g. 127.0.0.1)
   * @param ttl     Time to live for DNS record. Value of 1 is 'automatic'
   */
  def apply(
    dnsType: DnsRecordType,
    name: String,
    content: String,
    ttl: Int
  ): CreateDnsRecordData = new CreateDnsRecordData(dnsType, name, content, ttl)
}