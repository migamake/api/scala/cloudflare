package com.migamake.api.cloudflare.model

import java.time.ZonedDateTime

import com.migamake.api.cloudflare.model.enums.DnsRecordType.DnsRecordType
import play.api.libs.json.JsonConfiguration.Aux
import play.api.libs.json.{Reads, _}

/**
 * A Cloudflare DNS record response.
 *
 * @param id         DNS record identifier tag
 * @param dnsType    Record type
 * @param name       DNS record name
 * @param content    A valid IPv4 address
 * @param proxiable  Whether the record can be proxied by Cloudflare or not
 * @param proxied    Whether the record is receiving the performance and security benefits of Cloudflare
 * @param priority   Used with some records like MX and SRV to determine priority
 * @param ttl        Time to live for DNS record. Value of 1 is 'automatic'
 * @param locked     Whether this record can be modified/deleted (true means it's managed by Cloudflare)
 * @param zoneId     Zone identifier tag
 * @param zoneName   The domain of the record
 * @param createdOn  When the record was created
 * @param modifiedOn When the record was last modified
 * @param meta       Optional extra Cloudflare-specific information about the record.
 *                   This is a JSON object with no defined schema.
 * @param data       Optional metadata about the record
 *                   This is a JSON object with no defined schema.
 */
case class DnsRecord(
  id: String,
  dnsType: DnsRecordType,
  name: String,
  content: String,
  proxiable: Boolean,
  proxied: Boolean,
  priority: Option[Int],
  ttl: Int,
  locked: Boolean,
  zoneId: String,
  zoneName: String,
  createdOn: ZonedDateTime,
  modifiedOn: ZonedDateTime,
  data: Option[JsObject],
  meta: Option[JsObject],
)

private[cloudflare] object DnsRecord {
  private implicit val config: Aux[Json.MacroOptions] = JsonConfiguration(CloudflareApiJsonNaming)

  @SuppressWarnings(Array("org.wartremover.warts.Any"))
  implicit val reads: Reads[DnsRecord] = Json.reads[DnsRecord]
}
