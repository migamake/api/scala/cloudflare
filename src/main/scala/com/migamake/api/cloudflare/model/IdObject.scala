package com.migamake.api.cloudflare.model

import play.api.libs.json.{Json, Reads}

/**
 * A Cloudflare response object containing a single ID.
 *
 * @param id The record ID
 */
case class IdObject(
  id: String
)

object IdObject {
  implicit val reads: Reads[IdObject] = Json.reads[IdObject]
}