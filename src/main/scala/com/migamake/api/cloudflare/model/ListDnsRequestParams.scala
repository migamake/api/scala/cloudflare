package com.migamake.api.cloudflare.model

import com.migamake.api.cloudflare.client.ApiDefaultValues
import com.migamake.api.cloudflare.model.enums.DnsOrderingField.OrderingField
import com.migamake.api.cloudflare.model.enums.DnsRecordType.DnsRecordType
import com.migamake.api.cloudflare.model.enums.MatchType.MatchType
import com.migamake.api.cloudflare.model.enums.OrderingDirection.OrderingDirection

/**
 * List the DNS records for a zone, with optional filtering and sorting.
 *
 * 20 results per page are returned by default, which can be increased
 * to a maximum of 100.
 *
 * Note that if any DNS record fails to deserialise, the entire body will fail
 * to deserialize. Deserialization failures can be detected in the response body.
 *
 * Returns a status code of 200 for successful requests.
 *
 * @param matchType Whether to match all search requirements or at least one (any)
 * @param name      DNS record name
 *                  Max length: 255
 * @param order     Field to order records by
 * @param page      Page number of paginated results
 * @param perPage   Number of DNS records per page
 *                  Min value: 5
 *                  Max value: 100
 * @param content   DNS record content
 * @param dnsType   DNS record type
 * @param proxied   DNS record proxied status
 * @param direction Direction to order domains
 */
@SuppressWarnings(Array("org.wartremover.warts.DefaultArguments"))
case class ListDnsRequestParams(
  matchType: MatchType = ApiDefaultValues.matchType,
  name: Option[String] = None,
  order: Option[OrderingField] = None,
  page: Int = ApiDefaultValues.page,
  perPage: Int = ApiDefaultValues.perPage,
  content: Option[String] = None,
  dnsType: Option[DnsRecordType] = None,
  proxied: Option[Boolean] = None,
  direction: Option[OrderingDirection] = None
)

/**
 * A builder to construct [[com.migamake.api.cloudflare.model.ListDnsRequestParams]].
 *
 * The builder is *immutable*, so calls to set a property return a modified builder.
 *
 * For example,
 * ` params = ListDnsRequestParamsBuilder().setName(Some("name")).requestParams`
 *
 * @param build The built request parameters
 */
case class ListDnsRequestParamsBuilder private(build: ListDnsRequestParams) {

  def this() = this(ListDnsRequestParams())

  /**
   * Sets the parameter match type
   *
   * @param matchType Whether to match all search requirements or at least one (any)
   * @return A modified builder
   */
  def setMatchType(matchType: MatchType): ListDnsRequestParamsBuilder =
    ListDnsRequestParamsBuilder(build.copy(matchType = matchType))

  /**
   * Sets the DNS name
   *
   * @param name DNS record name
   *             Max length: 255
   * @return A modified builder
   */
  def setName(name: Option[String]): ListDnsRequestParamsBuilder =
    ListDnsRequestParamsBuilder(build.copy(name = name))

  /**
   * Sets the ordering field
   *
   * @param order Field to order records by
   * @return A modified builder
   */
  def setOrder(order: Option[OrderingField]): ListDnsRequestParamsBuilder =
    ListDnsRequestParamsBuilder(build.copy(order = order))

  /**
   * Sets the requested page
   *
   * @param page Page number of paginated results
   * @return A modified builder
   */
  def setPage(page: Int): ListDnsRequestParamsBuilder =
    ListDnsRequestParamsBuilder(build.copy(page = page))

  /**
   * Sets the number of requests per page
   *
   * @param perPage Number of DNS records per page
   *                Min value: 5
   *                Max value: 100
   * @return A modified builder
   */
  def setPerPage(perPage: Int): ListDnsRequestParamsBuilder =
    ListDnsRequestParamsBuilder(build.copy(perPage = perPage))

  /**
   * Sets the DNS content
   *
   * @param content DNS record content
   * @return A modified builder
   */
  def setContent(content: Option[String]): ListDnsRequestParamsBuilder =
    ListDnsRequestParamsBuilder(build.copy(content = content))

  /**
   * Sets the DNS type
   *
   * @param dnsType DNS record type
   * @return A modified builder
   */
  def setDnsType(dnsType: Option[DnsRecordType]): ListDnsRequestParamsBuilder =
    ListDnsRequestParamsBuilder(build.copy(dnsType = dnsType))

  /**
   * Sets the proxied status
   *
   * @param proxied DNS record proxied status
   * @return A modified builder
   */
  def setProxied(proxied: Option[Boolean]): ListDnsRequestParamsBuilder =
    ListDnsRequestParamsBuilder(build.copy(proxied = proxied))

  /**
   * Sets the ordering direction
   *
   * @param direction Direction to order domains
   * @return A modified builder
   */
  def setDirection(direction: Option[OrderingDirection]): ListDnsRequestParamsBuilder =
    ListDnsRequestParamsBuilder(build.copy(direction = direction))
}

object ListDnsRequestParamsBuilder {
  def apply(): ListDnsRequestParamsBuilder =
    ListDnsRequestParamsBuilder(build = ListDnsRequestParams())
}
