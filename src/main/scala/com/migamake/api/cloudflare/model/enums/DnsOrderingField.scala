package com.migamake.api.cloudflare.model.enums

/**
 * A field to order DNS entries by.
 */
object DnsOrderingField extends Enumeration {
  type OrderingField = Value
  val dnsType = Value("type")
  val name = Value("name")
  val content = Value("content")
  val ttl = Value("ttl")
  val proxied = Value("proxied")
}
