package com.migamake.api.cloudflare.model.enums

import play.api.libs.json.Reads

/**
 * Available DNS record types.
 */
object DnsRecordType extends Enumeration {
  type DnsRecordType = Value
  val A = Value("A")
  val AAAA = Value("AAAA")
  val CNAME = Value("CNAME")
  val TXT = Value("TXT")
  val SRV = Value("SRV")
  val LOC = Value("LOC")
  val MX = Value("MX")
  val NS = Value("NS")
  val SPF = Value("SPF")
  val CERT = Value("CERT")
  val DNSKEY = Value("DNSKEY")
  val DS = Value("DS")
  val NAPTR = Value("NAPTR")
  val SMIMEA = Value("SMIMEA")
  val SSHFP = Value("SSHFP")
  val TLSA = Value("TLSA")
  val URI = Value("URI")

  private[cloudflare] implicit val reads: Reads[Value] = Reads.enumNameReads(DnsRecordType)
}
