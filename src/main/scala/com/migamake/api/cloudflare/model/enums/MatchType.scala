package com.migamake.api.cloudflare.model.enums

/**
 *  Whether a search should match all (i.e. AND) or any (i.e. OR) of provided filters.
 */
object MatchType extends Enumeration {
  type MatchType = Value
  val all = Value("all")
  val any = Value("any")
}