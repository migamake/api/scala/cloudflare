package com.migamake.api.cloudflare.model.enums

/**
 * The sort direction for results, which can be ascending or descending.
 */
object OrderingDirection extends Enumeration {
  type OrderingDirection = Value
  val asc = Value("asc")
  val desc = Value("desc")
}
