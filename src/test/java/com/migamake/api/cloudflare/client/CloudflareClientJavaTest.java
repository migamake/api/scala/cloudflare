package com.migamake.api.cloudflare.client;

import com.migamake.api.cloudflare.model.*;
import com.migamake.api.cloudflare.model.enums.DnsRecordType;
import com.migamake.api.cloudflare.model.enums.MatchType;
import com.migamake.api.cloudflare.util.TestUtil;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import scala.Option;
import scala.collection.immutable.List;

import java.util.HashSet;
import java.util.Set;


@RunWith(JUnit4.class)
public class CloudflareClientJavaTest {

    private final SyncCloudflareDnsClient client = new SyncLoggingClient(TestUtil.authToken(), TestUtil.verboseMode());
    private final String zoneId = TestUtil.zoneId();

    private final static Set<String> idsToDelete = new HashSet<>();

    @AfterClass
    public static void deleteIds() throws Exception {
        SyncCloudflareDnsClient client = new SyncLoggingClient(TestUtil.authToken(), TestUtil.verboseMode());
        for (String id: idsToDelete) {
            client.deleteDnsRecord(TestUtil.zoneId(), id);
        }
    }

    @BeforeClass
    public static void cleanupRecords() {
        SyncCloudflareDnsClient client = new SyncLoggingClient(TestUtil.authToken(), TestUtil.verboseMode());
        TestUtil.deleteRecords(client, DnsRecordType.TXT(), TestUtil.domainName("java-test"));
        TestUtil.deleteRecords(client, DnsRecordType.TXT(), TestUtil.domainName("java-test2"));
        TestUtil.deleteRecords(client, DnsRecordType.MX(), TestUtil.domainName("java-test"));
    }

    @Test
    public void testAddingTxtDnsRecord() throws Exception {
        TestUtil.log("Start: 'Add TXT record Java test'");
        CreateDnsRecordData data = new CreateDnsRecordData(
                DnsRecordType.TXT(),
                TestUtil.domainName("java-test"),
                "foo",
                1
        );

        ClientResponse<CloudflareResponseContainer<DnsRecord>> response = client.addDnsRecord(zoneId, data);

        Assert.assertEquals(response.responseCode(), 200);
        Assert.assertTrue(response.body().isDefined());
        Assert.assertTrue(response.body().get().result().isDefined());

        DnsRecord createdRecord = response.body().get().result().get();
        idsToDelete.add(createdRecord.id());

        Assert.assertEquals(createdRecord.name(), TestUtil.domainName("java-test"));
        Assert.assertEquals(createdRecord.content(), "foo");
        Assert.assertEquals(createdRecord.dnsType(), DnsRecordType.TXT());
        Assert.assertEquals(createdRecord.ttl(), 1);
        Assert.assertTrue(createdRecord.priority().isEmpty());
        TestUtil.log("End: 'Add TXT record Java test'");
    }

    @Test
    public void testAddingAndDeletingMxDnsRecord() throws Exception {
        TestUtil.log("Start: 'Add/delete MX record Java test'");
        CreateDnsRecordData data = new CreateDnsRecordData(
                DnsRecordType.MX(),
                TestUtil.domainName("java-test"),
                "foo.example.com",
                1,
                Option.apply(1),
                Option.apply(false)
        );

        ClientResponse<CloudflareResponseContainer<DnsRecord>> response = client.addDnsRecord(zoneId, data);

        Assert.assertEquals(response.responseCode(), 200);
        Assert.assertTrue(response.body().isDefined());
        Assert.assertTrue(response.body().get().result().isDefined());

        DnsRecord createdRecord = response.body().get().result().get();

        Assert.assertEquals(createdRecord.name(), TestUtil.domainName("java-test"));
        Assert.assertEquals(createdRecord.content(), "foo.example.com");
        Assert.assertEquals(createdRecord.dnsType(), DnsRecordType.MX());
        Assert.assertEquals(createdRecord.ttl(), 1);
        Assert.assertEquals(createdRecord.priority(), Option.apply(1));

        ClientResponse<CloudflareResponseContainer<IdObject>> deleteResponse = client.deleteDnsRecord(zoneId, createdRecord.id());

        Assert.assertEquals(deleteResponse.responseCode(), 200);
        Assert.assertTrue(deleteResponse.body().isDefined());
        Assert.assertTrue(deleteResponse.body().get().result().isDefined());
        Assert.assertEquals(deleteResponse.body().get().result().get().id(), createdRecord.id());
        TestUtil.log("End: 'Add/delete MX record Java test'");
    }

    @Test
    public void testListingDnsRecords() throws Exception {
        TestUtil.log("Start: 'List records Java test'");
        CreateDnsRecordData data = new CreateDnsRecordData(
                DnsRecordType.TXT(),
                TestUtil.domainName("java-test2"),
                "foo",
                1
        );

        ClientResponse<CloudflareResponseContainer<DnsRecord>> addResponse = client.addDnsRecord(zoneId, data);

        Assert.assertEquals(addResponse.responseCode(), 200);

        ListDnsRequestParamsBuilder builder = new ListDnsRequestParamsBuilder();
        ListDnsRequestParams params = builder
                .setDnsType(Option.apply(DnsRecordType.TXT()))
                .setName(Option.apply(TestUtil.domainName("java-test2")))
                .setMatchType(MatchType.all())
                .build();

        ClientResponse<CloudflareResponseContainer<List<DnsRecord>>> response = client.listDnsRecords(zoneId, params);

        Assert.assertEquals(response.responseCode(), 200);
        Assert.assertTrue(response.body().isDefined());

        CloudflareResponseContainer<List<DnsRecord>> body = response.body().get();
        Assert.assertTrue(body.resultInfo().isDefined());
        Assert.assertEquals(body.resultInfo().get().page(), 1);

        Assert.assertTrue(body.result().isDefined());
        List<DnsRecord> result = body.result().get();
        Assert.assertFalse(result.isEmpty());
        TestUtil.log("End: 'List records Java test'");
    }
}
