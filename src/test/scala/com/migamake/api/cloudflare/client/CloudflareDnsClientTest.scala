package com.migamake.api.cloudflare.client

import com.migamake.api.cloudflare.model._
import com.migamake.api.cloudflare.model.enums.DnsOrderingField.OrderingField
import com.migamake.api.cloudflare.model.enums.DnsRecordType.DnsRecordType
import com.migamake.api.cloudflare.model.enums.MatchType.MatchType
import com.migamake.api.cloudflare.model.enums.OrderingDirection.OrderingDirection
import com.migamake.api.cloudflare.model.enums.{DnsOrderingField, DnsRecordType, MatchType, OrderingDirection}
import com.migamake.api.cloudflare.util.TestUtil
import org.scalatest.concurrent.{Eventually, ScalaFutures}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import org.scalatest.{AppendedClues, BeforeAndAfterAll}

import scala.collection.mutable
import scala.collection.parallel.ParSeq
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

@SuppressWarnings(Array(
  "org.wartremover.warts.NonUnitStatements",
  "org.wartremover.warts.Any",
  "org.wartremover.warts.OptionPartial",
  "org.wartremover.warts.Var",
  "org.wartremover.warts.DefaultArguments"
))
class CloudflareDnsClientTest extends AnyFunSuite with Matchers with AppendedClues with BeforeAndAfterAll with ScalaFutures with Eventually {

  override implicit val patienceConfig: PatienceConfig = PatienceConfig(15.seconds, 50.milliseconds)

  private def domainName(base: String): String = TestUtil.domainName(base)

  private val zoneId = TestUtil.zoneId
  private val client = new SyncLoggingClient(TestUtil.authToken, doLog = TestUtil.verboseMode)

  private var idsToDelete = mutable.Set[String]()

  private val aRecordSubDomain = "baz"
  private val cRecordSubDomain = "test"
  private val mxRecordSubDomain = "foo"

  private val cRecordContent = "migamake.com"
  private val aRecordContent = "1.1.1.1"
  private val mxRecordContent = domainName(mxRecordSubDomain)

  private val parallelRecordCreation = 20

  override def afterAll(): Unit = deleteIds()

  override def beforeAll(): Unit = {
    if (TestUtil.verboseMode) {
      log(s"Running with zone ID: $zoneId. Listing current DNS records:")
      val current = TestUtil.getAllDnsRecords(client)
      current.foreach(println)
      log(s"Found ${current.length.toString} DNS records")
    }
    log("Cleaning up previous records")
    cleanupRecords()
    log("Finished cleaning up previous records")
  }

  private def deleteIds(): Unit = {
    ParSeq(idsToDelete.toSeq: _*).foreach { id =>
      client.deleteDnsRecord(zoneId, id)
    }
    idsToDelete = mutable.Set[String]()
  }

  // Removes records matching the name and DNS type which will be created in the test
  private def cleanupRecords(): Unit = {
    val typedPrefixes = Map(
      DnsRecordType.TXT -> txtPrefixes,
      DnsRecordType.A -> Seq(aRecordSubDomain),
      DnsRecordType.CNAME -> Seq(cRecordSubDomain),
      DnsRecordType.MX -> Seq(mxRecordSubDomain)
    )
    val prefixes = for {
      (dnsType, prefixes) <- typedPrefixes.toSeq
      prefix <- prefixes
    } yield (dnsType, domainName(prefix))

    log(s"Found ${prefixes.size.toString} previous record(s) to cleanup")

    ParSeq(prefixes: _*).foreach {
      case (dnsType, name) =>
        TestUtil.deleteRecords(client, dnsType, name)
    }
  }

  private def txtPrefixes: Seq[String] = {
    val prefixes = Seq("foo", "bar")
    val numberedPrefixes = for {
      prefix <- prefixes
      suffix <- 0 to parallelRecordCreation
    } yield s"$prefix${suffix.toString}"

    val allPrefixes = prefixes ++ numberedPrefixes ++ Seq("baz")
    allPrefixes
  }

  private def log(msg: String): Unit = TestUtil.log(msg)

  test("Read DNS records from a zone") {
    log("Start 'Read DNS records from a zone'")
    val response = client.listDnsRecords(zoneId)
    response.responseCode shouldEqual 200
    response.body shouldBe defined
    response.rawBody.isRight shouldBe true

    val body: CloudflareResponseContainer[List[DnsRecord]] = response.body.get
    body.success shouldBe true
    body.errors shouldBe empty
    body.messages shouldBe empty
    body.resultInfo shouldBe defined
    body.result shouldBe defined
    log("End 'Read DNS records from a zone'")
  }

  test("Adding and deleting a new DNS record") {
    log("Start 'Adding and deleting a new DNS record'")
    val data = CreateDnsRecordData(
      DnsRecordType.TXT, domainName("foo"), "bar", 100, None, Some(false)
    )
    val response = client.addDnsRecord(zoneId, data)
    response.responseCode shouldEqual 200
    response.body shouldBe defined
    response.rawBody.isRight shouldBe true

    val body: CloudflareResponseContainer[DnsRecord] = response.body.get
    body.success shouldBe true
    body.errors shouldBe empty
    body.messages shouldBe empty
    body.resultInfo shouldBe empty
    body.result shouldBe defined

    val newRecord = body.result.get
    newRecord.name should startWith(data.name)
    newRecord.content shouldEqual data.content
    newRecord.dnsType shouldEqual data.dnsType
    newRecord.ttl shouldEqual data.ttl
    newRecord.proxied shouldEqual data.proxied.get

    val deleteResponse = client.deleteDnsRecord(zoneId, newRecord.id)
    deleteResponse.responseCode shouldEqual 200
    deleteResponse.body shouldBe defined
    deleteResponse.rawBody.isRight shouldBe true

    deleteResponse.body.get.result.get shouldEqual IdObject(newRecord.id)
    log("End 'Adding and deleting a new DNS record'")
  }

  test("Adding an MX DNS entry") {
    log("Start 'Adding an MX DNS entry'")
    val data = CreateDnsRecordData(
      DnsRecordType.MX, domainName(mxRecordSubDomain), mxRecordContent, 200, Some(40), Some(false)
    )
    val response = client.addDnsRecord(zoneId, data)
    response.responseCode shouldEqual 200 withClue response
    response.body shouldBe defined
    response.rawBody.isRight shouldBe true

    val body: CloudflareResponseContainer[DnsRecord] = response.body.get
    body.success shouldBe true
    body.errors shouldBe empty
    body.messages shouldBe empty
    body.resultInfo shouldBe empty
    body.result shouldBe defined

    val newRecord = body.result.get
    newRecord.name should startWith(data.name)
    newRecord.content shouldEqual data.content
    newRecord.dnsType shouldEqual data.dnsType
    newRecord.ttl shouldEqual data.ttl
    newRecord.proxied shouldEqual data.proxied.get
    newRecord.priority shouldEqual data.priority

    val deleteResponse = client.deleteDnsRecord(zoneId, newRecord.id)
    deleteResponse.responseCode shouldEqual 200
    deleteResponse.body shouldBe defined
    deleteResponse.rawBody.isRight shouldBe true

    deleteResponse.body.get.result.get shouldEqual IdObject(newRecord.id)
    log("End 'Adding an MX DNS entry'")
  }

  test("Adding a CNAME DNS entry") {
    log("Start 'Adding a CNAME DNS entry'")
    val data = CreateDnsRecordData(
      DnsRecordType.CNAME, domainName(cRecordSubDomain), cRecordContent, 1, None, Some(false)
    )
    val response = client.addDnsRecord(zoneId, data)
    response.responseCode shouldEqual 200 withClue response
    response.body shouldBe defined
    response.rawBody.isRight shouldBe true

    val body = response.body.get
    body.success shouldBe true
    body.errors shouldBe empty
    body.messages shouldBe empty
    body.resultInfo shouldBe empty
    body.result shouldBe defined

    val result = body.result.get
    idsToDelete.add(result.id)
    result.name should startWith(data.name)
    result.content shouldEqual data.content
    result.dnsType shouldEqual data.dnsType
    result.ttl shouldEqual data.ttl
    result.proxied shouldEqual data.proxied.get
    result.priority shouldEqual data.priority
    log("End 'Adding a CNAME DNS entry'")
  }

  test("Filtering DNS records") {
    log("Start 'Filtering DNS records'")
    val startingDnsIds = getAllDnsIds()

    val txt1 = addRecord(CreateDnsRecordData(DnsRecordType.TXT, domainName("bar"), "bar", 1, None, None))
    val txt2 = addRecord(CreateDnsRecordData(DnsRecordType.TXT, domainName("foo"), "bar2", 200, None, None))
    val a1 = addRecord(CreateDnsRecordData(DnsRecordType.A, domainName(aRecordSubDomain), aRecordContent, 300, None, Some(true)))

    val allDnsIds = Future(getAllDnsIds())
    val filterByName = Future(getAllDnsIds(name = Some(domainName("bar"))))
    val filterByType = Future(getAllDnsIds(dnsType = Some(DnsRecordType.A)))
    val filterByProxied = Future(getAllDnsIds(proxied = Some(true)))
    val orderedTtlAsc = Future(
      getAllDnsIds(
        dnsType = Some(DnsRecordType.TXT),
        order = Some(DnsOrderingField.ttl),
        direction = Some(OrderingDirection.asc)
      )
    )
    val orderedTtlDesc = Future(
      getAllDnsIds(
        dnsType = Some(DnsRecordType.TXT),
        order = Some(DnsOrderingField.ttl),
        direction = Some(OrderingDirection.desc)
      )
    )
    val matchingAnyFilter = Future(
      getAllDnsIds(
        matchType = MatchType.any,
        dnsType = Some(DnsRecordType.TXT),
        proxied = Some(true)
      )
    )

    whenReady(allDnsIds) { result =>
      result should contain allElementsOf List(txt1, txt2, a1)
    }

    whenReady(filterByName) { result =>
      result.diff(startingDnsIds) shouldEqual List(txt1)
    }

    whenReady(filterByType) { result =>
      result.diff(startingDnsIds) shouldEqual List(a1)
    }

    whenReady(filterByProxied) { result =>
      result.diff(startingDnsIds) shouldEqual List(a1)
    }

    whenReady(orderedTtlAsc) { result =>
      result.diff(startingDnsIds) shouldEqual List(txt1, txt2)
    }

    whenReady(orderedTtlDesc) { result =>
      result.diff(startingDnsIds) shouldEqual List(txt2, txt1)
    }

    whenReady(matchingAnyFilter) { result =>
      result.diff(startingDnsIds) should contain theSameElementsAs List(txt1, txt2, a1)
    }
    log("End 'Filtering DNS records'")
  }

  test("Creating many DNS records in parallel") {
    log("Start 'Creating many DNS records in parallel'")
    val iterations = 1 to parallelRecordCreation
    ParSeq(iterations: _*).map { i =>
      val name = domainName(s"foo${i.toString}")
      val data = CreateDnsRecordData(DnsRecordType.TXT, name, "bar", 1, None, None)
      val response = client.addDnsRecord(zoneId, data)
      response.responseCode shouldEqual 200 withClue response
      response.body shouldBe defined

      val result = response.body.get.result.get
      result.name should startWith(name)
      idsToDelete.add(result.id)
    }
    log("End 'Creating many DNS records in parallel'")
  }

  test("Creating many DNS records in parallel using the async client") {
    log("Start 'Creating many DNS records in parallel using the async client'")
    val asyncClient = new AsyncCloudflareDnsClient(TestUtil.authToken)
    val responses = ParSeq(1 to parallelRecordCreation: _*).map { i =>
      val name = domainName(s"bar${i.toString}")
      val data = CreateDnsRecordData(DnsRecordType.TXT, name, "bar", 1, None, None)
      val response = asyncClient.addDnsRecord(zoneId, data)
      response.map(r => (r, name))
    }
    val singleList = Future.sequence(responses.toList)
    whenReady(singleList) { responses =>
      // Add all the successful ids, in case one fails we still cleanup the created ids
      val ids = for {
        response <- responses
        body <- response._1.body.toList
        result <- body.result
      } yield result.id
      idsToDelete = idsToDelete ++ ids

      responses.foreach {
        case (response, name) =>
        response.responseCode shouldEqual 200 withClue response
        response.body shouldBe defined
        val result = response.body.get.result.get
        result.name should startWith(name)
      }
    }
    log("End 'Creating many DNS records in parallel using the async client'")
  }

  private def getAllDnsIds(
    matchType: MatchType = ApiDefaultValues.matchType,
    name: Option[String] = None,
    dnsType: Option[DnsRecordType] = None,
    proxied: Option[Boolean] = None,
    order: Option[OrderingField] = None,
    direction: Option[OrderingDirection] = None
  ): List[String] = TestUtil.getAllDnsRecords(
    client,
    matchType,
    name,
    dnsType,
    proxied,
    order,
    direction
  ).map(_.id)

  // Adds a new record and returns the ID
  private def addRecord(dnsRecord: CreateDnsRecordData): String = {
    val response = client.addDnsRecord(zoneId, dnsRecord)
    response.responseCode shouldEqual 200 withClue response
    val id = response.body.get.result.get.id
    idsToDelete.add(id)
    id
  }

}

private class SyncLoggingClient(authToken: String, doLog: Boolean) extends SyncCloudflareDnsClient(authToken) {

  private val log = RequestLoggingUtil(doLog)


  override def listDnsRecords(zoneId: String, requestParams: ListDnsRequestParams): ClientResponse[CloudflareResponseContainer[List[DnsRecord]]] = {
    val response = super.listDnsRecords(zoneId, requestParams)
    log.listRecords(zoneId, requestParams, response)
    response
  }

  override def addDnsRecord(zoneId: String, data: CreateDnsRecordData): ClientResponse[CloudflareResponseContainer[DnsRecord]] = {
    val response = super.addDnsRecord(zoneId, data)
    log.addRecord(zoneId, data, response)
    response
  }

  override def deleteDnsRecord(zoneId: String, dnsId: String): ClientResponse[CloudflareResponseContainer[IdObject]] = {
    val response = super.deleteDnsRecord(zoneId, dnsId)
    log.deleteRecord(zoneId, dnsId, response)
    response
  }
}

private class AsyncLoggingClient(authToken: String, doLog: Boolean) extends AsyncCloudflareDnsClient(authToken) {

  private val log = RequestLoggingUtil(doLog)

  override def listDnsRecords(zoneId: String, requestParams: ListDnsRequestParams): Future[ClientResponse[CloudflareResponseContainer[List[DnsRecord]]]] = {
    val futureResponse = super.listDnsRecords(zoneId, requestParams)
    futureResponse.map { response =>
      log.listRecords(zoneId, requestParams, response)
      response
    }
  }

  override def addDnsRecord(zoneId: String, data: CreateDnsRecordData): Future[ClientResponse[CloudflareResponseContainer[DnsRecord]]] = {
    val futureResponse = super.addDnsRecord(zoneId, data)
    futureResponse.map { response =>
      log.addRecord(zoneId, data, response)
      response
    }
  }

  override def deleteDnsRecord(zoneId: String, dnsId: String): Future[ClientResponse[CloudflareResponseContainer[IdObject]]] = {
    val futureResponse = super.deleteDnsRecord(zoneId, dnsId)
    futureResponse.map { response =>
      log.deleteRecord(zoneId, dnsId, response)
      response
    }
  }
}

private case class RequestLoggingUtil(doLog: Boolean) {
  def listRecords(
    zoneId: String,
    requestParams: ListDnsRequestParams,
    response: ClientResponse[CloudflareResponseContainer[List[DnsRecord]]]
  ): Unit = {
    val request = s"List record for zone $zoneId with params ${requestParams.toString}"
    printRequest(request, response)
  }

  def addRecord(
    zoneId: String,
    data: CreateDnsRecordData,
    response: ClientResponse[CloudflareResponseContainer[DnsRecord]]
  ):  Unit = {
    val request = s"Add record to $zoneId with data ${data.toString}"
    printRequest(request, response)
  }

  def deleteRecord(
    zoneId: String,
    dnsId: String,
    response: ClientResponse[CloudflareResponseContainer[IdObject]]
  ): Unit = {
    val request = s"Delete record $dnsId from zone $zoneId"
    printRequest(request, response)
  }

  private def responseToString[T](response: ClientResponse[CloudflareResponseContainer[T]]): String =
    response.body.map(_.toString).getOrElse(response.toString)

  private def printRequest[T](request: String, response: ClientResponse[CloudflareResponseContainer[T]]): Unit =
    if (doLog) {
      println(s"Request: $request\nResponse: ${responseToString(response)}\n")
    }
}