package com.migamake.api.cloudflare.model

import java.time.ZonedDateTime

import com.migamake.api.cloudflare.model.enums.DnsRecordType
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import play.api.libs.json.Json

class CloudflareResponseContainerReadsTest extends AnyFunSuite with Matchers {

  test("Reading successful cloudflare response") {
    val json =
      """
        |{
        |    "result": "foo",
        |    "success": true,
        |    "errors": [],
        |    "messages": [],
        |    "result_info": {
        |        "page": 1,
        |        "per_page": 20,
        |        "count": 1,
        |        "total_count": 1,
        |        "total_pages": 1
        |    }
        |}
        |""".stripMargin

    val response = Json.parse(json).as[CloudflareResponseContainer[String]]
    response shouldEqual CloudflareResponseContainer(
      true,
      Some("foo"),
      List(),
      List(),
      Some(ResultInfo(1, 20, 1, 1, 1))
    )
  }

  test("Reading unsuccessful response") {
    val json =
      """
        |{
        |    "success": false,
        |    "errors": [
        |        {
        |            "code": 10000,
        |            "message": "Authentication error"
        |        }
        |    ]
        |}
        |""".stripMargin

    val response = Json.parse(json).as[CloudflareResponseContainer[String]]
    response shouldEqual CloudflareResponseContainer(
      false,
      None,
      List(CloudflareError(10000, "Authentication error")),
      List(),
      None
    )
  }

  test("Reading a full response with a DNS record") {
    val json =
      """
        |{
        |    "result": [
        |        {
        |            "id": "e6bcaf2e0d00837ca62201f648b40a08",
        |            "zone_id": "c4839e311a49897d85075168bfcb61f9",
        |            "zone_name": "example.com",
        |            "name": "_domainconnect.omarfahmy.com",
        |            "type": "CNAME",
        |            "content": "connect.domains.google.com",
        |            "proxiable": true,
        |            "proxied": true,
        |            "ttl": 1,
        |            "locked": false,
        |            "meta": {
        |                "auto_added": false,
        |                "managed_by_apps": false,
        |                "managed_by_argo_tunnel": false,
        |                "source": "primary"
        |            },
        |            "created_on": "2020-11-08T06:41:20.76784Z",
        |            "modified_on": "2020-11-08T06:41:20.76784Z"
        |        }
        |    ],
        |    "success": true,
        |    "errors": [],
        |    "messages": [],
        |    "result_info": {
        |        "page": 1,
        |        "per_page": 20,
        |        "count": 1,
        |        "total_count": 1,
        |        "total_pages": 1
        |    }
        |}""".stripMargin

    val response = Json.parse(json).as[CloudflareResponseContainer[List[DnsRecord]]]
    val expectedMeta = Json.obj(
      "auto_added" -> false,
      "managed_by_apps" -> false,
      "managed_by_argo_tunnel" -> false,
      "source" -> "primary",
    )
    response shouldEqual CloudflareResponseContainer(
      success = true,
      Some(
        List(
          DnsRecord(
            "e6bcaf2e0d00837ca62201f648b40a08",
            DnsRecordType.CNAME,
            "_domainconnect.omarfahmy.com",
            "connect.domains.google.com",
            proxiable = true,
            proxied = true,
            None,
            ttl = 1,
            locked = false,
            "c4839e311a49897d85075168bfcb61f9",
            "example.com",
            ZonedDateTime.parse("2020-11-08T06:41:20.76784Z"),
            ZonedDateTime.parse("2020-11-08T06:41:20.76784Z"),
            None,
            Some(expectedMeta),
          )
        )
      ),
      List(),
      List(),
      Some(
        ResultInfo(
          1,
          20,
          1,
          1,
          1
        )
      )
    )
  }

}
