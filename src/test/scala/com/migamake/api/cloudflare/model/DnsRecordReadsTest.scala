package com.migamake.api.cloudflare.model

import java.time.ZonedDateTime

import com.migamake.api.cloudflare.model.enums.DnsRecordType
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import play.api.libs.json.Json

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements"))
class DnsRecordReadsTest extends AnyFunSuite with Matchers {

  test("Reading a DNS record") {
    Json.obj()
    val json =
      """
        |{
        |  "id": "372e67954025e0ba6aaa6d586b9e0b59",
        |  "type": "A",
        |  "name": "example.com",
        |  "content": "198.51.100.4",
        |  "proxiable": true,
        |  "proxied": false,
        |  "ttl": 120,
        |  "locked": false,
        |  "zone_id": "023e105f4ecef8ad9ca31a8372d0c353",
        |  "zone_name": "example.com",
        |  "created_on": "2014-01-01T05:20:00.12345Z",
        |  "modified_on": "2014-01-01T05:20:00.12345Z",
        |  "data": {},
        |  "meta": {
        |    "auto_added": true,
        |    "source": "primary"
        |  }
        |}""".stripMargin

    val response = Json.parse(json).as[DnsRecord]
    response shouldEqual DnsRecord(
      "372e67954025e0ba6aaa6d586b9e0b59",
      DnsRecordType.A,
      "example.com",
      "198.51.100.4",
      proxiable = true,
      proxied = false,
      None,
      120,
      locked = false,
      "023e105f4ecef8ad9ca31a8372d0c353",
      "example.com",
      ZonedDateTime.parse("2014-01-01T05:20:00.12345Z"),
      ZonedDateTime.parse("2014-01-01T05:20:00.12345Z"),
      Some(Json.obj()),
      Some(
        Json.obj(
          "auto_added" -> true,
          "source" -> "primary"
        )
      )
    )
  }

  test("Reading a DNS record with arbitrary data") {
    val json =
      """
        |{
        |  "id": "372e67954025e0ba6aaa6d586b9e0b59",
        |  "type": "A",
        |  "name": "example.com",
        |  "content": "198.51.100.4",
        |  "proxiable": true,
        |  "proxied": false,
        |  "ttl": 120,
        |  "locked": false,
        |  "zone_id": "023e105f4ecef8ad9ca31a8372d0c353",
        |  "zone_name": "example.com",
        |  "created_on": "2014-01-01T05:20:00.12345Z",
        |  "modified_on": "2014-01-01T05:20:00.12345Z",
        |  "data": {
        |    "foo": 1,
        |    "bar": {
        |      "baz": true
        |    }
        |  },
        |  "meta": {
        |    "auto_added": true,
        |    "source": "primary"
        |  }
        |}""".stripMargin

    val response = Json.parse(json).as[DnsRecord]
    val expectedData = Json.obj(
      "foo" -> 1,
      "bar" -> Json.obj(
        "baz" -> true
      )
    )
    val expectedMeta = Json.obj(
      "auto_added" -> true,
      "source" -> "primary"
    )
    response shouldEqual DnsRecord(
      "372e67954025e0ba6aaa6d586b9e0b59",
      DnsRecordType.A,
      "example.com",
      "198.51.100.4",
      proxiable = true,
      proxied = false,
      None,
      120,
      locked = false,
      "023e105f4ecef8ad9ca31a8372d0c353",
      "example.com",
      ZonedDateTime.parse("2014-01-01T05:20:00.12345Z"),
      ZonedDateTime.parse("2014-01-01T05:20:00.12345Z"),
      Some(expectedData),
      Some(expectedMeta)
    )
  }

  test("Reading a DNS record with no data or meta fields") {
    Json.obj()
    val json =
      """
        |{
        |  "id": "372e67954025e0ba6aaa6d586b9e0b59",
        |  "type": "A",
        |  "name": "example.com",
        |  "content": "198.51.100.4",
        |  "proxiable": true,
        |  "proxied": false,
        |  "ttl": 120,
        |  "locked": false,
        |  "zone_id": "023e105f4ecef8ad9ca31a8372d0c353",
        |  "zone_name": "example.com",
        |  "created_on": "2014-01-01T05:20:00.12345Z",
        |  "modified_on": "2014-01-01T05:20:00.12345Z"
        |}
        |""".stripMargin

    val response = Json.parse(json).as[DnsRecord]
    response shouldEqual DnsRecord(
      "372e67954025e0ba6aaa6d586b9e0b59",
      DnsRecordType.A,
      "example.com",
      "198.51.100.4",
      proxiable = true,
      proxied = false,
      None,
      120,
      locked = false,
      "023e105f4ecef8ad9ca31a8372d0c353",
      "example.com",
      ZonedDateTime.parse("2014-01-01T05:20:00.12345Z"),
      ZonedDateTime.parse("2014-01-01T05:20:00.12345Z"),
      None,
      None
    )
  }
}
