package com.migamake.api.cloudflare.model

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import play.api.libs.json.Json

class ResultInfoReadsTest extends AnyFunSuite with Matchers {

  private val json =
    """
      |{
      |        "page": 1,
      |        "per_page": 2,
      |        "count": 3,
      |        "total_count": 4,
      |        "total_pages": 5
      | }
      |""".stripMargin

  test("Reading results info") {
    val resultInfo = Json.parse(json).as[ResultInfo]
    resultInfo shouldEqual ResultInfo(1, 2, 3, 4, 5)
  }

}
