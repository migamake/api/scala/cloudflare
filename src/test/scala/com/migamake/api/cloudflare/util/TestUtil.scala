package com.migamake.api.cloudflare.util

import com.migamake.api.cloudflare.client.{ApiDefaultValues, SyncCloudflareDnsClient}
import com.migamake.api.cloudflare.model.enums.DnsOrderingField.OrderingField
import com.migamake.api.cloudflare.model.enums.DnsRecordType.DnsRecordType
import com.migamake.api.cloudflare.model.enums.MatchType
import com.migamake.api.cloudflare.model.enums.MatchType.MatchType
import com.migamake.api.cloudflare.model.enums.OrderingDirection.OrderingDirection
import com.migamake.api.cloudflare.model.{DnsRecord, ListDnsRequestParams}

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

@SuppressWarnings(Array("org.wartremover.warts.OptionPartial", "org.wartremover.warts.Throw", "org.wartremover.warts.DefaultArguments"))
object TestUtil {

  val authToken: String = getEnv("CLOUDFLARE_TOKEN")
  val zoneId: String = getEnv("CLOUDFLARE_ZONE")
  val baseDomainName: String = getEnv("CLOUDFLARE_DOMAIN")

  val verboseMode: Boolean = {
    val fromEnvVar = for {
      envVar <- Try(sys.env("VERBOSE_TESTS")).toOption
      bool <- Try(envVar.toBoolean).toOption
    } yield bool
    fromEnvVar.getOrElse(false)
  }

  private def getEnv(name: String): String =
    Try(sys.env(name)) match {
      case Failure(exception) =>
        println(s"Could not find environment variable $name")
        throw exception
      case Success(value) =>
        value
    }

  def log(msg: String): Unit =
    if (verboseMode) {
      println(msg)
    }

  def domainName(base: String): String =
    s"$base.scala-cloudflare-tests.$baseDomainName"

  def deleteRecords(client: SyncCloudflareDnsClient, dnsType: DnsRecordType, name: String): Unit = {
    val id =
      getAllDnsRecords(
        client,
        matchType = MatchType.all,
        dnsType = Some(dnsType),
        name = Some(name)
      ).map(_.id)
    id.foreach(client.deleteDnsRecord(zoneId, _))
  }

  def getAllDnsRecords(
    client: SyncCloudflareDnsClient,
    matchType: MatchType = ApiDefaultValues.matchType,
    name: Option[String] = None,
    dnsType: Option[DnsRecordType] = None,
    proxied: Option[Boolean] = None,
    order: Option[OrderingField] = None,
    direction: Option[OrderingDirection] = None
  ): List[DnsRecord] = {

    @tailrec
    def loop(acc: List[DnsRecord], page: Int): List[DnsRecord] = {
      val response = client.listDnsRecords(
        zoneId,
        ListDnsRequestParams(
          matchType = matchType,
          page = page,
          name = name,
          dnsType = dnsType,
          proxied = proxied,
          order = order,
          direction = direction,
          perPage = 100
        )
      )
      val results = response.body.get.result.get
      if (results.isEmpty || results.size < 100)
        acc ++ results
      else
        loop(acc ++ results, page + 1)
    }

    loop(List(), 1)
  }
}
